<?php
/**
 * Facebook Graph Api some sort of proxy script version 1.0
 * @author Radoslaw Kurowski
 * @email  radix@salvilines.eu
 */
class FacebookGraphApiSomeSortOfProxyScript
{
    const APP_ID       = ''; //put your client id here
    const APP_SECRET   = ''; //put your app secret here
    const ACCESS_TOKEN = ['access_token' => self::APP_ID.'|'.self::APP_SECRET];

    const API_PATH = 'https://graph.facebook.com/facebook/picture';

    private $getParams  = null;
    private $postParams = null;

    /**
     * Unpack incoming data.
     */
    public function __construct()
    {
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->postParams = $_POST;
        } else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            $this->getParams = $_GET;
        } else {
            echo 'Unsupported request type!';
            exit();
        }
    }


    /**
     * Resend data to fb graph and return a response.
     *
     * @return string
     */
    public function runProxy()
    {
        $result = false;
        if (!is_null($this->postParams)) {
            $options = [
                'http' => [
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query(array_merge($this->postParams))
                ]
            ];
            $context  = stream_context_create($options);
            $result = file_get_contents(self::API_PATH.'?access_token='.urlencode(self::ACCESS_TOKEN['access_token']), false, $context);
        } else {
            $query = '';
            foreach (array_merge($this->getParams, self::ACCESS_TOKEN) as $key => $value) {
                if ($query !== '') {
                    $query .= '&';
                }
                $query .= $key.'='.$value;
            }
            $result = file_get_contents(self::API_PATH.'?'.urlencode($query));
        }

        if ($result === false) {
            return 'Error!';
        } else {
            return $result;
        }
    }
}

//execute
$proxy = new FacebookGraphApiSomeSortOfProxyScript();
echo $proxy->runProxy();